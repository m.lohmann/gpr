import numpy as np
import matplotlib
matplotlib.use('WXAgg')
import h5py
import gpr_gssi as dzt
from glob import glob

if __name__ == "__main__":
    f = dzt.hdf5.get_handle_ro(glob("*.hdf5")[0])
    keys = f.keys()
    data        = np.array(f["cube"][...])
    data_global = np.array(f["cube_global"][...])
    data_local  = np.array(f["cube_local"][...])

    import matplotlib.pyplot as plt
    from matplotlib.colors import LogNorm
    from mayavi import mlab
    it,dimx,dimy = data.shape
    meters = f[keys[0]].attrs.get("meters")
    range_ns = f[keys[0]].attrs.get("range")
    X,Y,Z = np.mgrid[   slice(0.,it*0.6     ,it*1j  ),
                        slice(0.,meters     ,dimx*1j),
                        slice(0.,range_ns   ,dimy*1j)
                        ]

    mlab.figure(fgcolor=(0., 0., 0.), bgcolor=(1, 1, 1))
    src = mlab.pipeline.scalar_field(X,Y,Z,data_global)
    mlab.pipeline.iso_surface(src, contours=[data_global.min()+0.2*data_global.ptp(), ], opacity=0.1)
    mlab.pipeline.iso_surface(src, contours=[data_global.max()-0.2*data_global.ptp(), ],)
    surf = mlab.pipeline.image_plane_widget(src,
                                plane_orientation='x_axes',
                                slice_index=2
                            )
    mlab.axes(surf)

    mlab.figure(fgcolor=(0., 0., 0.), bgcolor=(1, 1, 1))
    src = mlab.pipeline.scalar_field(X,Y,Z,data_local)
    #mlab.pipeline.iso_surface(src, contours=[data_local.min()+0.2*data_global.ptp(), ], opacity=0.1)
    #mlab.pipeline.iso_surface(src, contours=[data_local.max()-0.2*data_global.ptp(), ],)
    surf = mlab.pipeline.image_plane_widget(src,
                                plane_orientation='x_axes',
                                slice_index=2
                            )
    mlab.axes(surf)

    mlab.show()
