import numpy as np
from glob import glob
#import h5py
import sys,os
import gpr_gssi as dzt
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button


if __name__ == "__main__":
    
    plt.close('all')    
    
    series = "OOO"
    k=3
    header, data = dzt.gssi.readgssi(series+'.PRJ/'+series+'_____'+'{0:03d}'.format(k)+'.DZT', antfreq=200, stack=1)
    for key in sorted(header):
        print(key, header[key])

    dimx,dimy = data.shape
    X,Y = np.mgrid[ slice(0.,header["meters"]   ,dimy*1j),
                    slice(0.,header["range"]    ,dimx*1j)]
    #plot the reflections
    fig, ax = plt.subplots()
    ax.pcolormesh(X,Y,data.T,cmap="seismic")
    ax.invert_yaxis()
    ax.set_xlabel("distance (m)")
    ax.set_ylabel("depth (ns)")
    ax.set_title(series+' '+str(k))
    
    plt.subplots_adjust(bottom=0.33)
    t = np.arange(0.0, header["meters"], 0.001)
    a0 = 10
    x0 = 3
    y0 = 10
    delta_f = 5.0
    s = a0*((t-x0)**(2))+y0
    l, = plt.plot(t, s, lw=2, color='k')
    plt.axis([0, 6, 50, 0])
    
    axcolor = 'lightgoldenrodyellow'
    axx = plt.axes([0.15, 0.1, 0.75, 0.03])#, facecolor=axcolor)
    axy = plt.axes([0.15, 0.15, 0.75, 0.03])#, facecolor=axcolor)
    axa = plt.axes([0.15, 0.2, 0.75, 0.03])#, facecolor=axcolor)
    
    sx = Slider(axx, 'x', 0.0, 6.0, valinit=x0)#, valstep=delta_f)
    sy = Slider(axy, 'y', 0.0, 30.0, valinit=y0)#, valstep=delta_f)
    sa = Slider(axa, 'a', 5, 30.0, valinit=a0)
    
    
    def update(val):
        a = sa.val
        x = sx.val
        y = sy.val
        l.set_ydata(a*((t-x)**2)+y)
        fig.canvas.draw_idle()
    sx.on_changed(update)
    sy.on_changed(update)
    sa.on_changed(update)
    
    resetax = plt.axes([0.8, 0.025, 0.1, 0.04])
    button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')
    
    
    def reset(event):
        sx.reset()
        sy.reset()
        sa.reset()
    button.on_clicked(reset)
    
    saveax = plt.axes([0.6, 0.025, 0.1, 0.04])
    savebutton = Button(saveax, 'Save', color=axcolor, hovercolor='0.975')    
    def save(event):
        print(sx.val, sy.val, sa.val)
        output = '{0:d}, {1:.3f}, {2:.3f}, {3:.3f} \n'.format(k, sx.val, sy.val, sa.val)
        with open(series+'.log', 'a') as f:
            f.write(output)
#        np.savetxt(f_handle, output, delimiter=', ', fmt='%.3f');
    savebutton.on_clicked(save)
    
    def on_press(event):
#        print('you pressed', event.button, event.xdata, event.ydata)
        if event.inaxes!=ax.axes: return
        sx.set_val(event.xdata)
        sy.set_val(event.ydata)

    cid = fig.canvas.mpl_connect('button_press_event', on_press)
    
    plt.show()
