import numpy as np
from glob import glob
import h5py
import sys,os
import gpr_gssi as dzt

if __name__ == "__main__":
    
    header, data = dzt.gssi.readgssi(sys.argv[1], antfreq=200, stack=1)
    for key in sorted(header):
        print key, header[key]

    #save as hdf5
    dzt.hdf5.export(header["filename"],header,data)

    from skimage import exposure
    from skimage.morphology import disk
    from skimage.filters import rank
    #global contrast
    data_global = exposure.equalize_hist(data)

    #local contrast
    selem = disk(30)
    data_local = rank.equalize(data,selem=selem)

    #plotting
    import matplotlib.pyplot as plt
    from matplotlib.colors import LogNorm
    #in numpy arrays the axis are flipped
    dimx,dimy = data.shape
    X,Y = np.mgrid[ slice(0.,header["meters"]   ,dimy*1j),
                    slice(0.,header["range"]    ,dimx*1j)]
    #plot the reflections
    f,ax=plt.subplots()
    ax.pcolor(X,Y,data.T,
                #norm=LogNorm(vmin=np.min(data),vmax=np.max(data)),
                cmap="inferno")
    ax.invert_yaxis()
    ax.set_xlabel("distance/m")
    ax.set_ylabel("depth/ns")
    ax.set_title("original")

    f,ax=plt.subplots()
    ax.pcolor(X,Y,data_global.T,
                #norm=LogNorm(vmin=np.min(data),vmax=np.max(data)),
                cmap="inferno")
    ax.invert_yaxis()
    ax.set_xlabel("distance/m")
    ax.set_ylabel("depth/ns")
    ax.set_title("global contrast")

    f,ax=plt.subplots()
    ax.pcolor(X,Y,data_local.T,
                #norm=LogNorm(vmin=np.min(data),vmax=np.max(data)),
                cmap="inferno")
    ax.invert_yaxis()
    ax.set_xlabel("distance/m")
    ax.set_ylabel("depth/ns")
    ax.set_title("local contrast")

    #wiggle plot
    f,ax=plt.subplots()
    ax.plot(data.T[600],np.arange(dimx),label="original")
    ax.plot(data_global.T[600],np.arange(dimx),label="global")
    ax.plot(data_local.T[600],np.arange(dimx),label="local")
    ax.invert_yaxis()
    ax.set_ylabel("depth/m")
    ax.legend(loc='best')

    plt.show()

    cut_hight = np.int(raw_input("Cut hight in pixel: "))

    data = data[cut_hight:]
    cut_time = np.arange(0.,header["range"],1./dimx)[cut_hight]

    dimx,dimy = data.shape
    X,Y = np.mgrid[ slice(0.,header["meters"]           ,dimy*1j),
                    slice(0.,header["range"]-cut_time   ,dimx*1j)]

    #global contrast
    data_global = exposure.equalize_hist(data)

    #local contrast
    selem = disk(30)
    data_local = rank.equalize(data,selem=selem)

    #plot the reflections
    f,ax=plt.subplots()
    ax.pcolor(X,Y,data.T,
                #norm=LogNorm(vmin=np.min(data),vmax=np.max(data)),
                cmap="inferno")
    ax.invert_yaxis()
    ax.set_xlabel("distance/m")
    ax.set_ylabel("depth/ns")

    f,ax=plt.subplots()
    ax.pcolor(X,Y,data_global.T,
                #norm=LogNorm(vmin=np.min(data),vmax=np.max(data)),
                cmap="inferno")
    ax.invert_yaxis()
    ax.set_xlabel("distance/m")
    ax.set_ylabel("depth/ns")
    ax.set_title("global contrast")

    f,ax=plt.subplots()
    ax.pcolor(X,Y,data_local.T,
                #norm=LogNorm(vmin=np.min(data),vmax=np.max(data)),
                cmap="inferno")
    ax.invert_yaxis()
    ax.set_xlabel("distance/m")
    ax.set_ylabel("depth/ns")
    ax.set_title("local contrast")

    #wiggle plot
    f,ax=plt.subplots()
    ax.plot(data.T[600],Y[0])
    ax.plot(data_global.T[600],Y[0],label="global")
    ax.plot(data_local.T[600],Y[0],label="local")
    ax.invert_yaxis()
    ax.set_ylabel("depth/ns")

    plt.show()
