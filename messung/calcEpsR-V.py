import numpy as np
from glob import glob
#import h5py
import sys,os
import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    
    plt.close('all')    
    
    series = "OOO"
    data  =np.loadtxt(series+'.log', delimiter=',')
    k = data[:,0]
    x0 = data[:,1]
    t0 = data[:,2]
    a = data[:,3]
    
    tCut = 8.5
    t0 = t0 - tCut
    
    v=2/np.sqrt((t0+a)**2-t0**2)
    epsR=(0.3/v)**2
    
    v_mean = np.mean(v)
    epsR_mean = np.mean(epsR)
    z=0.5*v_mean*t0
    plt.plot(k, epsR, 'x')