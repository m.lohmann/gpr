import numpy as np
from glob import glob
#import h5py
import sys,os
import gpr_gssi as dzt
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons


if __name__ == "__main__":
    
    folder_name = "OOO"
    header, data = dzt.gssi.readgssi(folder_name+'.PRJ/'+folder_name+'_____005.DZT', antfreq=200, stack=1)
    for key in sorted(header):
        print(key, header[key])

    #save as hdf5
    #dzt.hdf5.export(header["filename"],header,data)

    from skimage import exposure
    from skimage.morphology import disk
    from skimage.filters import rank
    #global contrast
    data_global = exposure.equalize_hist(data)

    #local contrast
    selem = disk(30)
    data_local = rank.equalize(data,selem=selem)

    #plotting
#    from matplotlib.colors import LogNorm
    #in numpy arrays the axis are flipped
    dimx,dimy = data.shape
    X,Y = np.mgrid[ slice(0.,header["meters"]   ,dimy*1j),
                    slice(0.,header["range"]    ,dimx*1j)]
    #plot the reflections
    fig, ax = plt.subplots()
    ax.pcolormesh(X,Y,data.T,
                #norm=LogNorm(vmin=np.min(data),vmax=np.max(data)),
                cmap="inferno")
    ax.invert_yaxis()
    ax.set_xlabel("distance/m")
    ax.set_ylabel("depth/ns")
    ax.set_title("original")
    
    plt.subplots_adjust(left=0.25, bottom=0.25)
    t = np.arange(0.0, header["meters"], 0.001)
    a0 = 10
    x0 = 3
    y0 = 10
    delta_f = 5.0
    s = a0*((t-x0)**(2))+y0
    l, = plt.plot(t, s, lw=2, color='red')
    plt.axis([0, 6, 50, 0])
    
    axcolor = 'lightgoldenrodyellow'
    axx = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor=axcolor)
    axy = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor=axcolor)
    axa = plt.axes([0.25, 0.2, 0.65, 0.03], facecolor=axcolor)
    
    sx = Slider(axx, 'x', 0.0, 6.0, valinit=x0)#, valstep=delta_f)
    sy = Slider(axy, 'y', 0.0, 30.0, valinit=y0)#, valstep=delta_f)
    sa = Slider(axa, 'a', 5, 20.0, valinit=a0)
    
    
    def update(val):
        a = sa.val
        x = sx.val
        y = sy.val
        l.set_ydata(a*((t-x)**2)+y)
        fig.canvas.draw_idle()
    sx.on_changed(update)
    sy.on_changed(update)
    sa.on_changed(update)
    
    resetax = plt.axes([0.8, 0.025, 0.1, 0.04])
    button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')
    
    
    def reset(event):
        sx.reset()
        sy.reset()
        sa.reset()
    button.on_clicked(reset)
    
    saveax = plt.axes([0.6, 0.025, 0.1, 0.04])
    savebutton = Button(saveax, 'Save', color=axcolor, hovercolor='0.975')    
    def save(event):
        print(sx.val, sy.val, sa.val)
    savebutton.on_clicked(save)
    
    plt.show()

#    f,ax=plt.subplots()
#    ax.pcolor(X,Y,data_global.T,
#                #norm=LogNorm(vmin=np.min(data),vmax=np.max(data)),
#                cmap="inferno")
#    ax.invert_yaxis()
#    ax.set_xlabel("distance/m")
#    ax.set_ylabel("depth/ns")
#    ax.set_title("global contrast")
#
#    f,ax=plt.subplots()
#    ax.pcolor(X,Y,data_local.T,
#                #norm=LogNorm(vmin=np.min(data),vmax=np.max(data)),
#                cmap="inferno")
#    ax.invert_yaxis()
#    ax.set_xlabel("distance/m")
#    ax.set_ylabel("depth/ns")
#    ax.set_title("local contrast")

    #wiggle plot
#    f,ax=plt.subplots()
#    ax.plot(data.T[100],np.arange(dimx),label="original")
#    ax.plot(data_global.T[100],np.arange(dimx),label="global")
#    ax.plot(data_local.T[100],np.arange(dimx),label="local")
#    ax.invert_yaxis()
#    ax.set_ylabel("depth/m")
#    ax.legend(loc='best')
#
#    plt.show()
"""
    cut_hight = np.int(raw_input("Cut hight in pixel: "))

    data = data[cut_hight:]
    cut_time = np.arange(0.,header["range"],1./dimx)[cut_hight]

    dimx,dimy = data.shape
    X,Y = np.mgrid[ slice(0.,header["meters"]           ,dimy*1j),
                    slice(0.,header["range"]-cut_time   ,dimx*1j)]

    #global contrast
    data_global = exposure.equalize_hist(data)

    #local contrast
    selem = disk(30)
    data_local = rank.equalize(data,selem=selem)

    #plot the reflections
    f,ax=plt.subplots()
    ax.pcolor(X,Y,data.T,
                #norm=LogNorm(vmin=np.min(data),vmax=np.max(data)),
                cmap="inferno")
    ax.invert_yaxis()
    ax.set_xlabel("distance/m")
    ax.set_ylabel("depth/ns")

    f,ax=plt.subplots()
    ax.pcolor(X,Y,data_global.T,
                #norm=LogNorm(vmin=np.min(data),vmax=np.max(data)),
                cmap="inferno")
    ax.invert_yaxis()
    ax.set_xlabel("distance/m")
    ax.set_ylabel("depth/ns")
    ax.set_title("global contrast")

    f,ax=plt.subplots()
    ax.pcolor(X,Y,data_local.T,
                #norm=LogNorm(vmin=np.min(data),vmax=np.max(data)),
                cmap="inferno")
    ax.invert_yaxis()
    ax.set_xlabel("distance/m")
    ax.set_ylabel("depth/ns")
    ax.set_title("local contrast")

    #wiggle plot
    f,ax=plt.subplots()
    ax.plot(data.T[100],Y[0])
    ax.plot(data_global.T[100],Y[0],label="global")
    ax.plot(data_local.T[100],Y[0],label="local")
    ax.invert_yaxis()
    ax.set_ylabel("depth/ns")

    plt.show()
"""